package model;

public abstract class Food extends Product implements Discountable {
    protected int amount;
    protected double price;
    protected boolean isVegetarian;
    protected int id;

    public double getDiscount() { //реализация метода
        return 0.0;
    }

    public int getAmount() {
        return this.amount;
    }

    public double getPrice() {
        return this.price;
    }

    public boolean getIsVegeterian() {
        return this.isVegetarian;
    }

    public int getId() {
        return this.id;
    }
}

