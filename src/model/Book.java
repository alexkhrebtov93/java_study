package model;
import model.constants.ProductType;

public class Book extends Product {

    private String isbn;
    private int pageNumber;
    private String author;

    public Book (String isbn, String title, int pageNumber, String author) {
        this.isbn = isbn;
        this.title = title;
        this.pageNumber = pageNumber;
        this.author = author;
        this.type = ProductType.BOOK;
    }

    public String getIso() {
        return this.isbn;
    }

    public int getPagesNumber() {
        return this.pageNumber;
    }

    public String getAuthor() {
        return this.author;
    }
}

