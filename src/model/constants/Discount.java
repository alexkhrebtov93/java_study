package model.constants;

public class Discount {
    public static final double DISCOUNT_ZERO = 0.0;
    public static final double DISCOUNT_TEN = 10.0;
    public static final double DISCOUNT_TWENTY = 20.0;
    public static final double DISCOUNT_THIRTY = 30.0;
    public static final double DISCOUNT_FOURTY = 40.0;
    public static final double DISCOUNT_FIFTY = 50.0;
    public static final double DISCOUNT_SIXTY = 60.0;
    public static final double DISCOUNT_SEVENTY = 70.0;
    public static final double DISCOUNT_EIGHTY = 80.0;
    public static final double DISCOUNT_NINTY = 90.0;
}
