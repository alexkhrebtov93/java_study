package model.constants;

public class ProductType {
    public static final String CAR = "car";
    public static final String HOUSE = "house";
    public static final String CLOTHES = "clothes";
    public static final String FOOD = "food";
    public static final String SHOES = "shoes";
    public static final String BOOK = "book";
}
