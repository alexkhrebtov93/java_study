package model.constants;

public class Role {
    public static final int ADMIN = 1;
    public static final int STAFF = 2;
    public static final int CLIENT = 3;
}
