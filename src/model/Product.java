package model;

public abstract class Product {
    protected String title;
    protected String type;

    public String getTitle() {
        return title;
    }
    
    public String getType() {
        return type;
    }
}
