package model;
import model.constants.ProductType;

public class Meat extends Food {
    public Meat(int amount, double price, int id) {
        this.isVegetarian = false;
        this.amount = amount;
        this.price = price;
        this.id = id;
        this.type = ProductType.FOOD;
    }
}
