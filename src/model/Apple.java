package model;

import model.constants.Colour;
import model.constants.Discount;
import model.constants.ProductType;

public class Apple extends Food {
    private String colour;
    public Apple(int amount, double price, String colour, int id) {
        this.isVegetarian = true;
        this.amount = amount;
        this.price = price;
        this.colour = colour;
        this.id = id;
        this.type = ProductType.FOOD;

    }
   public double getDiscount() { //Переопределение метода getDiscount()
        if (this.colour == Colour.RED) {
            return Discount.DISCOUNT_SIXTY; //Возвращаем констануту DISCOUNT_SIXTY
        }
        return Discount.DISCOUNT_ZERO; //Возвращаем констануту DISCOUNT_ZERO
    }
}

