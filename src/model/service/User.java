package model.service;


public abstract class User {
    protected String firstName;
    protected String lastName;
    protected String patronymic;
    protected int roleId;

    public User (String firstname, String lastName, String patronymic) { //конструктор сеттер
        this.firstName = firstname;
        this.lastName = lastName;
        this.patronymic = patronymic;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public String getPatronymic() {
        return this.patronymic;
    }
}