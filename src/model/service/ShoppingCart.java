package model.service;
import model.Food;
import java.util.ArrayList;

public class ShoppingCart {
    protected User user;
    protected ArrayList<Food> foodItems;//создаём поле с массивом Food[] который хранит объекты с типом Food

    public ShoppingCart(ArrayList<Food> foodItems) {
        this.foodItems = foodItems;
    }

    public double getItemsSum() {
        double allSum = 0;

        for (Food item : this.foodItems) {
            allSum += item.getAmount() * item.getPrice();
        }
        return allSum;
    }

    public double getItemsSumSale() {
        double allSum = 0;

        for (Food item : this.foodItems) {
            allSum += this.getItemSum(item) - (this.getItemSum(item) / 100 * item.getDiscount());
        }

        return allSum;
    }

    public double getSumVegetarianNoSale() {
        double allSum = 0;

        for (Food item : this.foodItems) {
            if (item.getIsVegeterian()) {
                allSum += item.getAmount() * item.getPrice();
            }
        }
        return allSum;
    }

    private double getItemSum(Food item) {
        return item.getAmount() * item.getPrice();
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getUserFullName() {
        return this.user.getFirstName() + " " + this.user.getLastName() + " " + this.user.getPatronymic();
    }

    public boolean deleteItem(int id) {
        return this.foodItems.removeIf(e -> e.getId() == id);
    }

    public int getSize() {
        return this.foodItems.size();
    }
}
